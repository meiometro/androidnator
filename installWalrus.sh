#!/bin/bash

DEVICENAME=$1

echo "$DEVICENAME installing Walrus.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/Walrus.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

adb -s $DEVICENAME shell cmd package set-home-activity "pt.shore.walrus/.activities.Activity_Main"

sleep 10

adb  -s $DEVICENAME reboot

echo "$DEVICENAME completed"
