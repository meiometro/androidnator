#!/bin/sh

DEVICENAME=$1

echo "$DEVICENAME removing myapps"
if adb -s $DEVICENAME shell pm uninstall -k --user 0 com.altice.android.myapps ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi


echo "$DEVICENAME removing swiftkey"
if adb -s $DEVICENAME shell pm uninstall -k --user 0 com.touchtype.swiftkey ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi



echo "$DEVICENAME removing freenetandmore"
if adb -s $DEVICENAME uninstall pt.meo.android.freenetandmore ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing androidtmndrive"
if adb -s $DEVICENAME uninstall com.ndrive.androidtmndrive ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing cloudpt"
if adb -s $DEVICENAME uninstall pt.sapo.android.cloudpt ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing sapoviver"
if adb -s $DEVICENAME uninstall pt.sapo.android.sapoviver ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing sapo24"
if adb -s $DEVICENAME uninstall pt.sapo.android.sapo24 ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing futebol"
if adb -s $DEVICENAME  uninstall pt.sapo.mobile.android.futebol ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing newsstand"
if adb -s $DEVICENAME uninstall pt.sapo.mobile.android.newsstand ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing meomobile"
if adb -s $DEVICENAME uninstall pt.ptinovacao.rma.meomobile ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing sapomobile"
if adb -s $DEVICENAME uninstall pt.sapo.mobile.android.sapomobile ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi
echo "$DEVICENAME THIS IS MEO FRIIIIIIIIIIIIIIIIIIIITO completed"
