#!/bin/sh

DEVICENAME=$1

echo "Fixing APN"

adb -s $DEVICENAME shell am start -n com.android.settings/com.android.settings.HWSettings

sleep 2

adb -s $DEVICENAME shell input tap 560 989

sleep 2

adb -s $DEVICENAME shell input tap 712 275

sleep 2

adb -s $DEVICENAME shell input tap 535 1118

sleep 2

adb -s $DEVICENAME shell input tap 1002 502

sleep 2

adb -s $DEVICENAME shell input tap 531 1716

sleep 5
