#!/bin/sh

#set -e

DEVICENAME=$1


echo "$DEVICENAME uninstall cityguide"
if adb -s $DEVICENAME shell pm uninstall net.gottsolutions.cityguide ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi


echo "$DEVICENAME uninstall dialer"
if adb -s $DEVICENAME shell pm uninstall net.gottsolutions.dialer ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME uninstall walrus"
if adb -s $DEVICENAME shell pm uninstall pt.shore.walrus ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing flightstats.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/flightstats.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

if [[ $(adb -s $DEVICENAME shell dumpsys telephony.registry | grep "vodafone") ]] ; then
  echo "Reception activation not needed"
else
  echo "$DEVICENAME call reception activation"
  if adb -s $DEVICENAME shell service call phone 2 s16 \"\" s16 \"*131%23\" ; then
    echo "Installation succedded"
  else
    echo "Installation failed"
  fi
fi

echo "$DEVICENAME installing com.sixt.reservation-1.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/com.sixt.reservation-1.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi


echo "$DEVICENAME installing dpc"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/net.gottsolutions.dpc-signed-new.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi


echo "$DEVICENAME installing launcher"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/net.gottsolutions.launcher.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing CurrencySimple.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/CurrencySimple.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing GoogleTranslate.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/GoogleTranslate.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi


echo "$DEVICENAME installing dialer"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/net.gottsolutions.dialer.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing cityguide"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/net.gottsolutions.cityguide.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME setting DPC as device owner"
adb -s $DEVICENAME shell dpm set-device-owner net.gottsolutions.dpc/.ServiceDeviceAdminReceiver

sleep 2

adb -s $DEVICENAME shell cmd package set-home-activity "net.gottsolutions.launcher/.ui.Activity_Main"

sleep 10

adb  -s $DEVICENAME reboot

echo "$DEVICENAME completed"
