#!/bin/sh

DEVICENAME=$1

echo "$DEVICENAME removing CurrencySimple.apk"
if adb -s $DEVICENAME uninstall com.currencyapp.currencyandroid ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing GoogleTranslate.apk"
if adb -s $DEVICENAME uninstall com.google.android.apps.translate ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing gottDialer.apk"
if adb -s $DEVICENAME uninstall net.gottsolutions.dialer ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing YahooWeather.apk"
if adb -s $DEVICENAME uninstall com.yahoo.mobile.client.android.weather ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing cityguide.apk"
if adb -s $DEVICENAME uninstall HuaweiP8Lite/cityguide.apk ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing CutTheRope.apk"
if adb -s $DEVICENAME uninstall com.zeptolab.ctr.ads ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing Flow.apk"
if adb -s $DEVICENAME  uninstall com.bigduckgames.flow ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing GoogleChrome.apk"
if adb -s $DEVICENAME uninstall com.android.chrome ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing Moovit.apk"
if adb -s $DEVICENAME uninstall com.tranzmate ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing Ndrive.apk"
if adb -s $DEVICENAME uninstall com.veezitgps ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing Walrus.apk"
if adb -s $DEVICENAME uninstall pt.shore.walrus ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

echo "$DEVICENAME removing DPC.apk"
if adb -s $DEVICENAME uninstall net.gottsolutions.dpc ; then
  echo " uninstallation succedded"
else
  echo " uninstallation failed"
fi

adb -s $DEVICENAME reboot

echo "$DEVICENAME completed"
