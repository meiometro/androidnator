﻿using AndroidnatorSetup.ChainOfCommands;
using System;
using System.Collections.Generic;
using System.Windows;

namespace AndroidnatorSetup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Install_Click(object sender, RoutedEventArgs e)
        {
            ToggleButtons();

            ChainManager chainManager = new ChainManager(new List<IChainHandler>
            {
                new CygwinSetupChainHandler(),
                new CopyFilesChainHandler()
            });

            chainManager.Execute(() => ToggleButtons());
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ToggleButtons()
        {
            Install.IsEnabled = !Install.IsEnabled;
            Exit.IsEnabled = !Exit.IsEnabled;

            if (Loading.Visibility == Visibility.Hidden)
            {
                Loading.Visibility = Visibility.Visible;
            }
            else
            {
                Loading.Visibility = Visibility.Hidden;
            }
        }
    }
}
