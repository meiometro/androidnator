﻿using System;
using System.Collections.Generic;

namespace AndroidnatorSetup.ChainOfCommands
{
    public class ChainManager
    {
        private readonly List<IChainHandler> _chainHandlerList;

        public ChainManager(List<IChainHandler> chainHandlerList)
        {
            _chainHandlerList = chainHandlerList;
        }

        public void Execute(Action executeEndedAction = null)
        {
            foreach (var chainHandler in _chainHandlerList)
            {
                chainHandler.Execute();
            }

            executeEndedAction?.Invoke();
        }
    }
}
