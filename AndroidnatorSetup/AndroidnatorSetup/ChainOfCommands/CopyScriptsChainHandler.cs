﻿using AndroidnatorSetup.Utils;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AndroidnatorSetup.ChainOfCommands
{
    public class CopyFilesChainHandler : IChainHandler
    {
        private const string ANDROIDINATOR_PATH = @"C:\androidnator\";

        public void Execute()
        {
            if (!Directory.Exists(ANDROIDINATOR_PATH))
            {
                Directory.CreateDirectory(ANDROIDINATOR_PATH);
            }

            DirectoryUtils.DirectoryCopy(Directory.GetCurrentDirectory() + "\\Scripts", ANDROIDINATOR_PATH);
            DirectoryUtils.DirectoryCopy(Directory.GetCurrentDirectory() + @"\AndroidModels\HuaweiP8Lite", ANDROIDINATOR_PATH + "HuaweiP8Lite");
            DirectoryUtils.DirectoryCopy(Directory.GetCurrentDirectory() + "\\Maps", ANDROIDINATOR_PATH + @"Maps");

            File.Copy(Directory.GetCurrentDirectory() + @"\Androidnator.ico", ANDROIDINATOR_PATH + "Androidnator.ico", true);

            string lnkFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "StartAndroidnator.lnk");
            File.Copy(Directory.GetCurrentDirectory() + @"\Resources\StartAndroidnator.lnk", lnkFileName, true);
        }
    }
}
