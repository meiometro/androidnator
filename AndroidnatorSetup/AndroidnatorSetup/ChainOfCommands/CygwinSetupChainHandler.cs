﻿using System;
using System.Diagnostics;
using System.IO;

namespace AndroidnatorSetup.ChainOfCommands
{
    public class CygwinSetupChainHandler : IChainHandler
    {
        private const string CYGWIN_PATH = @"C:\cygwin64";

        public void Execute()
        {
            if (!Directory.Exists(CYGWIN_PATH))
            {
                Directory.CreateDirectory(CYGWIN_PATH);
            }

            string site = "http://cygwin.mirror.constant.com/";
            string localdir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/cygwin64";

            Process cygwinSetupProcess = new Process();
            cygwinSetupProcess.StartInfo = new ProcessStartInfo
            {
                FileName = @"Dependencies\cygwin\setup-x86_64.exe",
                Arguments = $@"-q -D -L -d -g -o -s {site} -l ""{localdir}"" -R ""{CYGWIN_PATH}"" -C Base"
            };
            
            cygwinSetupProcess.Start();
            cygwinSetupProcess.WaitForExit();
        }
    }
}
