﻿namespace AndroidnatorSetup.ChainOfCommands
{
    public interface IChainHandler
    {
        void Execute();
    }
}