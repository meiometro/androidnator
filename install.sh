#!/bin/sh

#set -e

DEVICENAME=$1

echo "$DEVICENAME enabling mobile data"
if adb -s $DEVICENAME shell settings put global data_roaming 1 ; then
echo "Installation succedded"
else
echo "Installation failed"
fi

if [[ $(adb -s $DEVICENAME shell dumpsys telephony.registry | grep "vodafone") ]] ; then
  echo "Reception activation not needed"
else
  echo "$DEVICENAME call reception activation"
  if adb -s $DEVICENAME shell service call phone 2 s16 \"\" s16 \"*131%23\" ; then
    echo "Installation succedded"
  else
    echo "Installation failed"
  fi
fi

echo "$DEVICENAME installing DPC.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/net.gottsolutions.dpc-signed-new.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME setting DPC as device owner"
adb -s $DEVICENAME shell dpm set-device-owner net.gottsolutions.dpc/.ServiceDeviceAdminReceiver

sleep 2

echo "$DEVICENAME installing CurrencySimple.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/CurrencySimple.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing GooglePlayServices.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/GooglePlayServices.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing GoogleTranslate.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/GoogleTranslate.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing gottDialer.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/gottDialer.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing cityguide.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/cityguide.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing CutTheRope.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/CutTheRope.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing Flow.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/Flow.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing GoogleChrome.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/GoogleChrome.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing Moovit.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/Moovit.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

echo "$DEVICENAME installing Ndrive.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/Ndrive.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi
