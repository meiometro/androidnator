#!/bin/sh

#set -e

DEVICENAME=$1

echo "$DEVICENAME removing Ndrive"
if adb -s $DEVICENAME shell pm uninstall com.veezitgps ; then
  echo "succedded"
else
  echo "failed"
fi


echo "$DEVICENAME installing Ndrive.apk"
if adb -s $DEVICENAME install -r -g Hisense/Ndrive.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

adb -s $DEVICENAME shell am force-stop com.veezitgps/com.ndrive.mi9.Cor3MapActivity

sleep 10

adb -s $DEVICENAME shell am start -n com.veezitgps/com.ndrive.mi9.Cor3MapActivity

sleep 10

adb -s $DEVICENAME shell input tap 857 1147

sleep 2

adb -s $DEVICENAME shell input tap 765 1097

sleep 2

adb -s $DEVICENAME shell input tap 87 150

sleep 2

adb -s $DEVICENAME shell input tap 540 847

sleep 2

adb -s $DEVICENAME shell input tap 870 325

sleep 2

adb -s $DEVICENAME shell input tap 619 340

sleep 12

adb -s $DEVICENAME shell input tap 933 405

adb -s $DEVICENAME shell input tap 956 413

adb -s $DEVICENAME push Maps/products sdcard/Android/data/com.veezitgps/files
