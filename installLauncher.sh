#!/bin/bash

DEVICENAME=$1

echo "$DEVICENAME installing net.gottsolutions.launcher.apk"
if adb -s $DEVICENAME install -r -g HuaweiP8Lite/net.gottsolutions.launcher.apk ; then
  echo "Installation succedded"
else
  echo "Installation failed"
fi

adb -s $DEVICENAME shell cmd package set-home-activity "net.gottsolutions.launcher/.ui.Activity_Main"

sleep 10

adb  -s $DEVICENAME reboot

echo "$DEVICENAME completed"
