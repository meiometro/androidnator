#!/bin/bash

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"

#    ______                   __                      __        __                       __
#   /      \                 |  \                    |  \      |  \                     |  \
#  |  $$$$$$\ _______    ____| $$  ______    ______   \$$  ____| $$ _______    ______  _| $$_     ______    ______
#  | $$__| $$|       \  /      $$ /      \  /      \ |  \ /      $$|       \  |      \|   $$ \   /      \  /      \
#  | $$    $$| $$$$$$$\|  $$$$$$$|  $$$$$$\|  $$$$$$\| $$|  $$$$$$$| $$$$$$$\  \$$$$$$\\$$$$$$  |  $$$$$$\|  $$$$$$\
#  | $$$$$$$$| $$  | $$| $$  | $$| $$   \$$| $$  | $$| $$| $$  | $$| $$  | $$ /      $$ | $$ __ | $$  | $$| $$   \$$
#  | $$  | $$| $$  | $$| $$__| $$| $$      | $$__/ $$| $$| $$__| $$| $$  | $$|  $$$$$$$ | $$|  \| $$__/ $$| $$
#  | $$  | $$| $$  | $$ \$$    $$| $$       \$$    $$| $$ \$$    $$| $$  | $$ \$$    $$  \$$  $$ \$$    $$| $$
#   \$$   \$$ \$$   \$$  \$$$$$$$ \$$        \$$$$$$  \$$  \$$$$$$$ \$$   \$$  \$$$$$$$   \$$$$   \$$$$$$  \$$
#

trap killgroup SIGINT

killgroup(){

  echo killing...

  kill 0

}

GREEN='\033[00;32m'
WHITE='\033[00;00m'
CYAN='\033[00;36m'
YELLOW='\033[00;33m'
RED='\033[00;31m'

function showMenu() {
cat << "EOF"
                                         `.:/+oosssoo++/-.`                                         
                                   `-+shmMMMMMMMMMMMMMMMMMNmhs/.                                    
                                -ohNMMMMMMMNmdhhyyyyhddNNMMMMMMMNh+.                                
                             -smMMMMMNds+:.`            `.:+ydNMMMMMmo.                             
                          `+dMMMMMdo-`                        `:sdMMMMMd/`                          
                        `oNMMMMd+.                                .omMMMMm+`                        
                       /mMMMMy-                                      :hMMMMm:                       
                     .hMMMMh-                                          :dMMMMy`                     
                    :NMMMm/                                              +NMMMm-                    
                   /NMMMh`                                                .dMMMN:                   
                  /MMMMs`    ohyo+:.                           `.:+syh/    `hMMMN:                  
                 -NMMMy     `mMMMMMMNdy+:.`              `.:oydNMMMMMMd     `hMMMm.                 
                `dMMMd`      dMMMMMMMMMMMMNdyo/-.``.-/oymNMMMMMMMMMMMMy      .mMMMy                 
                +MMMN-       sMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM+       /MMMM:                
                dMMMh        /MMMMMMMMMMMMMMMMMMNyyNMMMMMMMMMMMMMMMMMN-       `mMMMy                
               .NMMM+         :hNMMMMMMMMMMMMMMy`  .hMMMMMMMMMMMMMMNy-         sMMMN`               
               :MMMM-           `-+ydNMMMMMMMMs     `yMMMMMMMMNdy+-`           /MMMN.               
               :MMMM-                 `-:/+os/       `+so+/:.`                 :MMMM-               
               -MMMM:                                                          /MMMN.               
               .NMMM+                     `..`        `..`                     sMMMm`               
                hMMMd`                  .oNMMMNmd/+dmNMMMm+`                  `mMMMs                
                /MMMM/                .sNMMMMMMMMMMMMMMMMMMNs.                +MMMN-                
                 hMMMm.             -yMMMMMMMMMMMMMMMMMMMMMMMNy-             -NMMMs                 
                 .mMMMh`          :dMMMMMmso+/:--..-::/+oymMMMMMh-          .mMMMd`                 
                  :NMMMh`         sMMMmo-                  -smMMM+         .dMMMm.                  
                   :NMMMd-        .Nh-                        -dm`        :mMMMm-                   
                    -dMMMNo`       ss                          h+       `sNMMMh.                    
                     `sMMMMm/      .o                          s`     `+mMMMNo`                     
                       -dMMMMd+`    -                          -    `+mMMMMh.                       
                         /dMMMMNy:`                              `:yNMMMMh:                         
                           :yNMMMMNy+.                       `-+hNMMMMNs-                           
                             `+hNMMMMMNhs+:.`          `-:+sdNMMMMMNy/`                             
                                `:sdMMMMMMMMMNNmmddmmNNMMMMMMMMNdo:`                                
                                    `-+shmNMMMMMMMMMMMMMMNmhs+-`                                    
                                           `-:://////:-.`                                     
										   
EOF
	echo -e "${CYAN}*********************************************${WHITE}"
	echo -e "${CYAN}**${YELLOW} 1)${CYAN} Install Hisense ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 2)${CYAN} Install Vodafone ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 3)${CYAN} Install Honor 6X ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 4)${CYAN} Install Huawei P8Lite ${WHITE}"
  echo -e "${CYAN}**${YELLOW} 5)${CYAN} Install Huawei P8Lite V2 ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 6)${CYAN} Install Doogee ${WHITE}"
  echo -e "${CYAN}**${YELLOW} 7)${CYAN} Install Maps ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 8)${CYAN} Remove Applications ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 9)${CYAN} Remove Maps ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 10)${CYAN} Remove Meo BloatWare ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 11)${CYAN} Root Hisense ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 12)${CYAN} Uninstall Hisense ${WHITE}"
	echo -e "${CYAN}**${YELLOW} 13)${CYAN} Install p8Lite Quick Upgrade ${WHITE}"
  echo -e "${CYAN}**${YELLOW} 0)${CYAN} Quit ${WHITE}"
	echo -e "${CYAN}*********************************************${WHITE}"
	echo -e "${YELLOW}Please enter a menu option and enter or ${RED}enter to exit. ${WHITE}"
	read opt
}

function infoMessage() {
	MESSAGE=${@:-"${WHITE}Error: No message passed"}
	echo -e "${GREEN}${MESSAGE}${WHITE}"
}

function errorMessage() {
	MESSAGE=${@:-"${WHITE}Error: No message passed"}
	echo -e "${Red}${MESSAGE}${WHITE}"
}

function waitForKeyPress() {
	read -rsp $'Press any key to continue...\n' -n 1
}

function waitForKeyPressWithTimeout() {
	read -rsp $'Press any key or wait 5 seconds to continue...\n' -n 1 -t 5;
}

function waitForEscape() {
	read -rsp $'Press escape to continue...\n' -d $'\e'
}

function waitForEnter() {
	read -rsp $'Press enter to continue...\n'
}

function installHisense() {
  infoMessage "Removing Applications!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
  	DEVICENAME=${DEVICEOPTIONS[$i]}
    ./Hisense/install.sh $DEVICENAME &
  done
  wait
  infoMessage "Removing applications completed!"
  waitForEnter
  clear
}

function installp8LiteQuick() {
  infoMessage "Removing Applications!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
  	DEVICENAME=${DEVICEOPTIONS[$i]}
    ./HuaweiP8Lite/installV2_quick.sh $DEVICENAME &
  done
  wait
  infoMessage "Done!"
  waitForEnter
  clear
}

function installVodafone() {
	infoMessage "Installing Vodafone apps!"
	infoMessage "Instalation completed!"
	waitForEnter
	clear
}

function installHonor6x() {
	infoMessage "Installing Huawei Honor 6X apps!"
	infoMessage "Instalation completed!"
	waitForEnter
	clear
}

function installP8Lite() {
	infoMessage "Instaling Huawei P8Lite apps!"
  startTime=$(date +"%T")
  echo "Current time : $now"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/meo_BW_remover.sh $DEVICENAME &
	done
  wait
  cat << "EOF"
       _,.
     ,` -.)
    '( _/'-\\-.               
   /,|`--._,-^|            ,     
   \_| |`-._/||          ,'|       
     |  `-, / |         /  /      
     |     || |        /  /       
      `r-._||/   __   /  /  
  __,-<_     )`-/  `./  /
 '  \   `---'   \   /  / 
     |           |./  /  
     /           //  /     
 \_/' \         |/  /         
  |    |   _,^-'/  /              
  |    , ``  (\/  /_        
   \,.->._    \X-=/^         
   (  /   `-._//^`  
    `Y-.____(__}              
     |     {__)           
           ()`     
EOF
	for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/install.sh $DEVICENAME &
	done
  wait
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
    adb -s $DEVICENAME shell am force-stop com.veezitgps/com.ndrive.mi9.Cor3MapActivity

    sleep 10

    adb -s $DEVICENAME shell am start -n com.veezitgps/com.ndrive.mi9.Cor3MapActivity
	done
  wait
  waitForEnter
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/hackMaps.sh $DEVICENAME &
	done
  wait
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		adb -s $DEVICENAME push Maps/products sdcard/Android/data/com.veezitgps/files &
	done
  wait
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/installWalrus.sh $DEVICENAME &
	done
	wait
  endTime=$(date +"%T")
  echo "Start time : $startTime"
  echo "End time : $endTime"
	infoMessage "Instalation completed!"
	cat << "EOF"
───────────────────░█▓▓▓█░▇▆▅▄▃▂
──────────────────░█▓▓▓▓▓█░▇▆▅▄▃▂
─────────────────░█▓▓▓▓▓█░▇▆▅▄▃▂
──────────░░░───░█▓▓▓▓▓▓█░▇▆▅▄▃▂ 
─────────░███░──░█▓▓▓▓▓█░▇▆▅▄▃▂
───────░██░░░██░█▓▓▓▓▓█░▇▆▅▄▃▂
──────░█░░█░░░░██▓▓▓▓▓█░▇▆▅▄▃▂
────░██░░█░░░░░░█▓▓▓▓█░▇▆▅▄▃▂
───░█░░░█░░░░░░░██▓▓▓█░▇▆▅▄▃▂
──░█░░░░█░░░░░░░░█▓▓▓█░▇▆▅▄▃▂
──░█░░░░░█░░░░░░░░█▓▓▓█░▇▆▅▄▃▂
──░█░░█░░░█░░░░░░░░█▓▓█░▇▆▅▄▃▂
─░█░░░█░░░░██░░░░░░█▓▓█░▇▆▅▄▃▂
─░█░░░░█░░░░░██░░░█▓▓▓█░▇▆▅▄▃▂
─░█░█░░░█░░░░░░███▓▓▓▓█░▇▆▅▄▃▂
░█░░░█░░░██░░░░░█▓▓▓▓▓█░▇▆▅▄▃▂
░█░░░░█░░░░█████▓▓▓▓▓█░▇▆▅▄▃▂
░█░░░░░█░░░░░░░█▓▓▓▓▓█░▇▆▅▄▃▂
░█░█░░░░██░░░░█▓▓▓▓▓█░▇▆▅▄▃▂
─░█░█░░░░░████▓▓▓▓██░▇▆▅▄▃▂
─░█░░█░░░░░░░█▓▓██▓█░▇▆▅▄▃▂
──░█░░██░░░██▓▓█▓▓▓█░▇▆▅▄▃▂
───░██░░███▓▓██▓█▓▓█░▇▆▅▄▃▂
────░██▓▓▓███▓▓▓█▓▓▓█░▇▆▅▄▃▂
──────░█▓▓▓▓▓▓▓▓█▓▓▓█░▇▆▅▄▃▂
──────░█▓▓▓▓▓▓▓▓▓▓▓▓▓█░▇▆▅▄▃▂
EOF
  waitForEnter
	clear
}

function installP8LiteV2() {
	infoMessage "Instaling Huawei P8Lite apps!"
  startTime=$(date +"%T")
  echo "Current time : $now"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/meo_BW_remover.sh $DEVICENAME &
	done
  wait
  cat << "EOF"
       _,.
     ,` -.)
    '( _/'-\\-.               
   /,|`--._,-^|            ,     
   \_| |`-._/||          ,'|       
     |  `-, / |         /  /      
     |     || |        /  /       
      `r-._||/   __   /  /  
  __,-<_     )`-/  `./  /
 '  \   `---'   \   /  / 
     |           |./  /  
     /           //  /     
 \_/' \         |/  /         
  |    |   _,^-'/  /              
  |    , ``  (\/  /_        
   \,.->._    \X-=/^         
   (  /   `-._//^`  
    `Y-.____(__}              
     |     {__)           
           ()`     
EOF
	for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/installV2.sh $DEVICENAME &
	done
  wait
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
    adb -s $DEVICENAME shell am force-stop com.veezitgps/com.ndrive.mi9.Cor3MapActivity

    sleep 10

    adb -s $DEVICENAME shell am start -n com.veezitgps/com.ndrive.mi9.Cor3MapActivity
	done
  wait
  waitForEnter
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/hackMaps.sh $DEVICENAME &
	done
  wait
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		adb -s $DEVICENAME push Maps/products sdcard/Android/data/com.veezitgps/files &
	done
  wait
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./HuaweiP8Lite/installLauncher.sh $DEVICENAME &
	done
	wait
  endTime=$(date +"%T")
  echo "Start time : $startTime"
  echo "End time : $endTime"
	infoMessage "Instalation completed!"
		cat << "EOF"
───────────────────░█▓▓▓█░▇▆▅▄▃▂
──────────────────░█▓▓▓▓▓█░▇▆▅▄▃▂
─────────────────░█▓▓▓▓▓█░▇▆▅▄▃▂
──────────░░░───░█▓▓▓▓▓▓█░▇▆▅▄▃▂
─────────░███░──░█▓▓▓▓▓█░▇▆▅▄▃▂
───────░██░░░██░█▓▓▓▓▓█░▇▆▅▄▃▂
──────░█░░█░░░░██▓▓▓▓▓█░▇▆▅▄▃▂
────░██░░█░░░░░░█▓▓▓▓█░▇▆▅▄▃▂
───░█░░░█░░░░░░░██▓▓▓█░▇▆▅▄▃▂
──░█░░░░█░░░░░░░░█▓▓▓█░▇▆▅▄▃▂
──░█░░░░░█░░░░░░░░█▓▓▓█░▇▆▅▄▃▂
──░█░░█░░░█░░░░░░░░█▓▓█░▇▆▅▄▃▂
─░█░░░█░░░░██░░░░░░█▓▓█░▇▆▅▄▃▂
─░█░░░░█░░░░░██░░░█▓▓▓█░▇▆▅▄▃▂
─░█░█░░░█░░░░░░███▓▓▓▓█░▇▆▅▄▃▂
░█░░░█░░░██░░░░░█▓▓▓▓▓█░▇▆▅▄▃▂
░█░░░░█░░░░█████▓▓▓▓▓█░▇▆▅▄▃▂
░█░░░░░█░░░░░░░█▓▓▓▓▓█░▇▆▅▄▃▂
░█░█░░░░██░░░░█▓▓▓▓▓█░▇▆▅▄▃▂
─░█░█░░░░░████▓▓▓▓██░▇▆▅▄▃▂
─░█░░█░░░░░░░█▓▓██▓█░▇▆▅▄▃▂
──░█░░██░░░██▓▓█▓▓▓█░▇▆▅▄▃▂
───░██░░███▓▓██▓█▓▓█░▇▆▅▄▃▂
────░██▓▓▓███▓▓▓█▓▓▓█░▇▆▅▄▃▂
──────░█▓▓▓▓▓▓▓▓█▓▓▓█░▇▆▅▄▃▂
──────░█▓▓▓▓▓▓▓▓▓▓▓▓▓█░▇▆▅▄▃▂
EOF
  waitForEnter
	clear
}

function installDoogee() {
  infoMessage "Instaling Doogee apps!"
	infoMessage "Instalation completed!"
  waitForEnter
	clear
}

function installMaps() {
  infoMessage "Instaling Maps!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
		./install_maps.sh $DEVICENAME &
	done
	wait
  infoMessage "Instalation completed!"
  waitForEnter
	clear
}

function removeMaps() {
  infoMessage "Removing Maps!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
		DEVICENAME=${DEVICEOPTIONS[$i]}
	  adb -s $DEVICENAME shell rm -rf sdcard/Android/data/com.veezitgps/files/products/ &
	done
	wait
  infoMessage "Removing maps completed!"
  waitForEnter
	clear
}

function removeApplications() {
  infoMessage "Removing Applications!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
  	DEVICENAME=${DEVICEOPTIONS[$i]}
    ./remove.sh $DEVICENAME &
  done
  wait
  infoMessage "Removing applications completed!"
  waitForEnter
  clear
}

function removeMeoBloatWare() {
  infoMessage "Removing Applications!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
  	DEVICENAME=${DEVICEOPTIONS[$i]}
    ./HuaweiP8Lite/meo_BW_remover.sh $DEVICENAME &
  done
  wait
  infoMessage "Removing applications completed!"
  waitForEnter
  clear
}

function rootHisense() {
  infoMessage "Rooting!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
  	DEVICENAME=${DEVICEOPTIONS[$i]}
    ./Hisense/Root.sh $DEVICENAME &
  done
  wait
  infoMessage "Removing applications completed!"
  waitForEnter
  clear
}

function uninstallHisense() {
  infoMessage "Removing Applications!"
  for (( i=0; i<${#DEVICEOPTIONS[*]}; i++ )); do
  	DEVICENAME=${DEVICEOPTIONS[$i]}
    ./Hisense/Uninstall.sh $DEVICENAME &
  done
  wait
  infoMessage "Removing applications completed!"
  waitForEnter
  clear
}

DEVICEOPTIONS=()
readarray DEVICEOPTIONS -t <<< "$(adb devices | awk 'NR>2 { print deviceName} {deviceName = $1}')"

clear
showMenu
while [ opt != '' ]
do
  case $opt in
    1)
  		clear
  		installHisense
      showMenu
      ;;
    2)
      clear
      installVodafone
      showMenu
      ;;
    3)
      clear
      installHonor6x
      showMenu
      ;;
    4)
      clear
      installP8Lite
      showMenu
      ;;
    5)
      clear
      installP8LiteV2
      showMenu
      ;;
    6)
      clear
      installDoogee
      showMenu
      ;;
    7)
      clear
      installMaps
      showMenu
      ;;
  	8)
      clear
      removeApplications
      showMenu
      ;;
    9)
      clear
      removeMaps
      showMenu
      ;;
	10)
      clear
	  cat << "EOF"
                                                              _( (~\
       _ _                        /                          ( \> > \
   -/~/ / ~\                     :;                \       _  > /(~\/
  || | | /\ ;\                   |l      _____     |;     ( \/    > >
  _\\)\)\)/ ;;;                  `8o __-~     ~\   d|      \      //
 ///(())(__/~;;\                  "88p;.  -. _\_;.oP        (_._/ /
(((__   __ \\   \                  `>,% (\  (\./)8"         ;:'  i
)))--`.'-- (( ;,8 \               ,;%%%:  ./V^^^V'          ;.   ;.
((\   |   /)) .,88  `: ..,,;;;;,-::::::'_::\   ||\         ;[8:   ;
 )|  ~-~  |(|(888; ..``'::::8888oooooo.  :\`^^^/,,~--._    |88::  |
 |\ -===- /|  \8;; ``:.      oo.8888888888:`((( o.ooo8888Oo;:;:'  |
 |_~-___-~_|   `-\.   `        `o`88888888b` )) 888b88888P""'     ;
 ; ~~~~;~~         "`--_`.       b`888888888;(.,"888b888"  ..::;-'
   ;      ;              ~"-....  b`8888888:::::.`8888. .:;;;''
      ;    ;                 `:::. `:::OOO:::::::.`OO' ;;;''
 :       ;                     `.      "``::::::''    .'
    ;                           `.   \_              /
  ;       ;                       +:   ~~--  `:'  -';
                                   `:         : .::/  
      ;                            ;;+_  :::. :..;;;  
                                   ;;;;;;,;;;;;;;;,;
EOF
      removeMeoBloatWare
      showMenu
      ;;
	11)
      clear
      rootHisense
      showMenu
      ;;
	  
	12)
      clear
      uninstallHisense
      showMenu
      ;;
	  
	13)
      clear
      installp8LiteQuick
      showMenu
      ;;
    0)
      clear
      break
      ;;
	
    *)
      clear
      #echo invalid option
      showMenu
      ;;
  esac
done
